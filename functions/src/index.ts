import * as functions from "firebase-functions";
import * as admin from "firebase-admin";

admin.initializeApp();
const firestore = admin.firestore();

exports.Caducado = functions.pubsub.schedule("every 5 minutes")
    .onRun((context) => {
      console.log("producto Expirado");
      const path = "Productos/";
      const today = new Date();
      
      console.log(today);
      return firestore.collection(path).where("productos.fechaEx", "<"
          , today).get().then((res) => {
        console.log("hay productos caducados");
      });
    });
