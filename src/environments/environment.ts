// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
 firebaseConfig : {
  apiKey: "AIzaSyBTNiVTvOyW1xcQ5--lYybouFx9-OSvUbk",
  authDomain: "prodexpired-5fe05.firebaseapp.com",
  databaseURL: "https://prodexpired-5fe05.firebaseio.com",
  projectId: "prodexpired-5fe05",
  storageBucket: "prodexpired-5fe05.appspot.com",
  messagingSenderId: "197872613505",
  appId: "1:197872613505:web:955d6dddcfff91bd083580",
  measurementId: "G-J9FDD9060S"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
