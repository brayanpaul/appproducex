

export interface Producto {
    id: string;
    nombre: string;
    descripcion: string;
    fechaEx: any;
    fechaEL: any;
    stock: number;
    precio: number;
    categoria: categoria;
    foto: string;
    estado:string;
}


export interface Categoria {
    id: string;
    nombre: string;
}
export interface Usuario {
    uid: string;
    nombre: string;
    email: string;
    contra: string;
}

export type categoria =  'Enlatado' | 'Galletas' | 'Lacteos';