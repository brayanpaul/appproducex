import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export class BasedatosService {
    constructor(public Firestore: AngularFirestore) { }


    crearDocument<tipo>(data: tipo, enlace: string, id: string) {
        const ref = this.Firestore.collection<tipo>(enlace);
        return ref.doc(id).set(data);
    }
    getDoc<tipo>(path:string, id: string){
        const collection = this.Firestore.collection<tipo>(path);
        return collection.doc(id).valueChanges();
    }
s    

    creatId() {
        return this.Firestore.createId();

    }

    delateDocument(path: string, id: string) {
        const collection = this.Firestore.collection(path);
        return collection.doc(id).delete();
    }
    UpdateDocument(data: any, path: string, id: string) {
        const collection = this.Firestore.collection(path);
        return collection.doc(id).update(data);
    }
    getCollectionChanges<tipo>(enlace: string): Observable<tipo[]> {
        const ref = this.Firestore.collection<tipo>(enlace);
        return ref.valueChanges();
    }
     //para llama a categoria:"lacteos" busqueda
    getCollectionQuery<tipo>(path:string,parametro:string,condicion:any, busqueda: string){
        const collection =this.Firestore.collection<tipo>(
          path, ref=> ref.where(parametro,condicion,busqueda));
          return collection.valueChanges();

    }


    editDocument() {

    }
}