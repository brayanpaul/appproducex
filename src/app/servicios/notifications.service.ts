import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FirebaseauthService } from './firebaseauth.service';
import { BasedatosService } from './basedaros.service';
//import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed,
  LocalNotificationActionPerformed
} from '@capacitor/core';

const { PushNotifications } = Plugins;
const { LocalNotifications } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(public platform: Platform,
    public BasedatosService: BasedatosService,
    public FirebaseauthService: FirebaseauthService,
    public router: Router,
   // private http: HttpClient
   ) {
    this.stateUser();
    this.inicializar();
  }

  stateUser() {

  }
  inicializar() {
    if (this.platform.is('capacitor')) {
      PushNotifications.requestPermission().then(result => {
        console.log('PushNotifications.requestPermission()');
        if (result.granted) {
          console.log('permisos concedidos');
          PushNotifications.register();
          this.addListeners();
        } else {

        }
      });

    } else {
      console.log('PushNotifications.requestPermission() -> no es movil');
    }
  }
  addListeners() {
    LocalNotifications.schedule({
      notifications: [
        {
          title: 'notificacion Local',
          body: 'notification.body',
          id: 1,

        }
      ]
    });
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        this.guardarToken(token.value);
        console.log('The token is:', token);
      });
    PushNotifications.addListener('registrationError',
      (error: any) => {
        console.log('Error con registration', error)
      }
    );
    //Primer plano notificacion local
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        console.log('Push received: ', notification);
        LocalNotifications.schedule({
          notifications: [
            {
              title: 'notificacion Local',
              body: notification.body,
              id: 1,
              extra: {
                data: notification.data
              }
            }
          ]
        });
      }
    );
    //clic en la notificacion pushh
    PushNotifications.addListener ('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        console.log('Push action performed en segundo plano:', notification);
        this.router.navigate(['/Home']);
      });
   //capturar un evento en notificaciones locales
         LocalNotifications.addListener ('localNotificationActionPerformed',
      (notification: LocalNotificationActionPerformed) => {
        console.log('Push action performed en segundo plano:', notification);
        this.router.navigate(['/Login']);
      });
}
async guardarToken(token: any){
const Uid = await this.FirebaseauthService.getUid();
if (Uid){
  console.log('Guardar Token Firebase ->', Uid);
  const path = '/Productos';
  const userUpdate = {
    token: token,
      };
      this.BasedatosService.UpdateDocument(userUpdate, path, Uid);
      console.log('guardar TokenFirebase()->', userUpdate,path, Uid);
}
}
}