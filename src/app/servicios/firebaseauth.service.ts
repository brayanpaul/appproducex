import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Usuario } from '../models/interfaces';
@Injectable({
  providedIn: 'root'
})
export class FirebaseauthService {
  public islogged: any =false;

  constructor(public auth: AngularFireAuth) { 
    auth.authState.subscribe (Usuario=>(this.islogged =Usuario));
    this.getUid();
  }
  async login (email:string, password: string){
   
  return await this.auth.signInWithEmailAndPassword(email, password);
  }catch(error){
    console.log('Error on login', error);
  }

  logout() {
    this.auth.signOut();
  }
async registrar(email: string, password: string){
    try{
    return await this.auth.createUserWithEmailAndPassword(email, password);
  }catch(error){
    console.log('Error on login', error);
  }
}
 async getUid(){
const user = await this.auth.currentUser;
if (user == null){
  return null;
}else{
  return user.uid;
}
}
stateAuth(){
 return this.auth.authState;
}
}
