import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { LoginComponent } from './paginas/login/login.component';
import { NotificationsService } from './servicios/notifications.service';
import { FirebaseauthService } from './servicios/firebaseauth.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  admin=false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private NotificationsService: NotificationsService,
    private FirebaseauthService:FirebaseauthService
  ) {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.getUid();
    });
  }
  //funcion si el id es igual a admin muestre el menu productos
  getUid(){
    this.FirebaseauthService.stateAuth().subscribe(res =>{
      if (res !== null){
        if(res.uid === 'IKbEOrdloiQXS9V4InK75YquUGC2'){
         this.admin=true;
        }else{
          this.admin=false;
        }
      }
    })

  }
  closeMenu(){
    this.menu.close('custom');
  }
}
