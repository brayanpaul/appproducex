import { CategoriasComponent } from './paginas/categorias/categorias.component';
import { ListaPComponent } from './paginas/lista-p/lista-p.component';
import { ProductosComponent } from './paginas/productos/productos.component';
import { LoginComponent } from './paginas/login/login.component';
import { HomeComponent } from './paginas/home/home.component';
import { NgModule } from '@angular/core';
import {AuthGuard} from './guard/auth.guard';
import { canActivate } from '@angular/fire/auth-guard'


import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { map } from 'rxjs/operators';
//comparar usuario id y si no cumple no hay como navegar a esa ruta 
const isAdmin =(next:any) => map((user:any)=> !!user && 'IKbEOrdloiQXS9V4InK75YquUGC2'=== user.uid);

const routes: Routes = [
  {
    path: 'Home',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Categorias',
    component: CategoriasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'Listap',
    component: ListaPComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'Login',
    component: LoginComponent
  },
  
  {
    path: 'Productos',
    component: ProductosComponent,
    canActivate: [AuthGuard], ...canActivate (isAdmin)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
