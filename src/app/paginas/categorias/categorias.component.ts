import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { BasedatosService } from './../../servicios/basedaros.service';
import { Categoria } from './../../models/interfaces';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-contactos',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.scss'],
})
export class CategoriasComponent implements OnInit {


  @Input() productoenable = false;
   // Categoria //
   categoria: Categoria[] = [];
   itemsC: Categoria[] = [];
   loading: any;

   newCategoria: Categoria = {
    id: this.database.creatId(),
    nombre: '',
  }

  private pathC = 'Categoria/';
  enablenewCategoria = false;


  constructor( public database: BasedatosService, public toastController: 
    ToastController, public loadingController: LoadingController,public alertController: AlertController,
     public BasedatosService: BasedatosService ) { }
  ngOnInit() {

    this.getItemsC();
  }
//Agrega Categoria
addCategoria() {
  this.presentLoading('Guardandoo..');
  this.database.crearDocument(this.newCategoria, this.pathC, this.newCategoria.id).then(res => {
    this.newCategoria={
     id:this.database.creatId(),
     nombre:'',
    };
    this.loading.dismiss();
  }).catch(error =>{});
  this.presentToast('Guardado Correctamente', 2000 );
  
}

//Muestra datos "Categoria"
getItemsC() {
  const enlace = 'Categoria';
  this.BasedatosService.getCollectionChanges<Categoria>(enlace).subscribe(res => {
    this.itemsC = res;
  });
}


//Borra y muestra interfas de agregar "Categoria"
nuevoC() {
  this.enablenewCategoria = true;
  this.newCategoria = {
    id: this.database.creatId(),
    nombre: '',
  };
}

  // Para mostrar mensaje 
  async presentToast(mensaje: string, tiempo: number) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: tiempo
    })
    toast.present();
  }

  async presentLoading(message: string) {
    this.loading = await this.loadingController.create({
      cssClass:'my-custom-class',
      message: message,
  
    });
    await this.loading.present();
    //await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }
// elimina datos categoria
  async DelateMCategoria(categoria: Categoria){
    const alert = await this.alertController.create({
       cssClass: 'my-custom-class',
       header: 'Peligro',
       message: 'Seguro que desea <strong>ELIMINAR</strong> esta categoria',
       buttons: [
         {
           text: 'Cancelar',
           role: 'cancel',
           cssClass: 'secondary',
           handler: (blah) => {
             console.log('Confirm Cancel: blah');
           }
         }, {
           text: 'Si',
           handler: () => {
            this.BasedatosService.delateDocument(this.pathC,categoria.id);
           }
         }
       ]
     });
 
     await alert.present();
   }

}
