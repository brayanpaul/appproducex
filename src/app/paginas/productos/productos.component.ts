import { FirestorageService } from './../../servicios/firestorage.service';
import { BasedatosService } from './../../servicios/basedaros.service';
import { Component, OnInit, Input } from '@angular/core';
import { Producto } from 'src/app/models/interfaces';
import { ToastController, LoadingController } from '@ionic/angular';
import { Categoria } from '../../models/interfaces';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss'],
})
export class ProductosComponent implements OnInit {

  
  productos: Producto []=[];
  private path='Productos/';
  loading: any;
  newImage= '';
  newFile='';

  //Categorias

  categoria: Categoria[] = [];
  itemsC: Categoria[] = [];
 
  newProducto: Producto={
    id: this.database.creatId(),
    nombre: '',
    descripcion: '',
    fechaEx: '',
    fechaEL:'' ,
    stock:null,
    precio:null, 
    categoria: 'Lacteos',
    foto: '',
    estado:''
   };
  


  constructor( public loadingController: LoadingController, 
    public database: BasedatosService,public BasedatosService: BasedatosService, 
    public toastController: ToastController,public firestorageService:FirestorageService) {

  }

  ngOnInit() { 
    console.log(this.productos);
    this.getItemsC();
  }


 async addProducto(){
  this.presentLoading();
const path='Productos';
const name =this.newProducto.nombre;
const res= await this.firestorageService.uploadImage(this.newFile,path,name);
this.newProducto.foto=res;
this.newProducto.fechaEx =new Date(this.newProducto.fechaEx);
this.newProducto.fechaEL =new Date(this.newProducto.fechaEL);
console.log(this.newProducto);
  this.database.crearDocument(this.newProducto, this.path,this.newProducto.id).then(res => {
    this.newProducto={
      id: this.database.creatId(),
      nombre: '',
      descripcion: '',
      fechaEx: '',
      fechaEL:'' ,
      stock:null,
      precio:null, 
      categoria: 'Lacteos',
      foto: '',
      estado:''
     };
    this.loading.dismiss();
  }).catch(error =>{});
  this.presentToast('Guardado Correctamente', 2000 );
}


//Ctegoria
getItemsC() {
  const enlace = 'Categoria';
 this.BasedatosService.getCollectionChanges<Categoria>(enlace).subscribe(res => {
   this.itemsC = res;
  });
}



async presentToast(mensaje:string, tiempo :number){
  const toast= await this.toastController.create({
    message: mensaje,
    duration:tiempo
  })
  toast.present();
}

//Carga imagen
async newImagenUpload( event: any){
if(event.target.files && event.target.files[0]){
this.newFile= event.target.files[0];
  const reader= new FileReader();
  reader.onload=((image)=>{
    this.newImage=image.target.result as string;
  });
  reader.readAsDataURL(event.target.files[0]);
}
}


async presentLoading() {
  this.loading = await this.loadingController.create({
    cssClass:'my-custom-class',
    message: 'Gargando Producto :)',

  });
  await this.loading.present();
  //await loading.onDidDismiss();
  // console.log('Loading dismissed!');
}
  

}
