import { Usuario } from './../../models/interfaces';
import { FirebaseauthService } from './../../servicios/firebaseauth.service';
import { BasedatosService } from './../../servicios/basedaros.service';
import { FirestorageService } from './../../servicios/firestorage.service';
import { MenuController, ToastController, LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {Router}from "@angular/router"


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  usuario: Usuario = {
    uid: '',
    nombre: '',
    email: '',
    contra: '',
  };
  newFile: any;
  uid = '';
  suscriberUserInfo: Subscription;
  IngresarEnable = false;
  IngresarEnableH = false;
  Home = false;
  loading: any;

  constructor(public MenuController: MenuController,
    public FirebaseauthService: FirebaseauthService,
    public FirestorageService: FirestorageService,
    public BasedatosService: BasedatosService,
    public database: BasedatosService,
    public toastController: ToastController,
    public loadingController: LoadingController,
    public router:Router,

  ) {
    this.FirebaseauthService.stateAuth().subscribe(res => {
      console.log(res);
      if (res !== null) {
        this.uid = res.uid;
        this.getUserInfo(this.uid);
      } else {
        this.initUsuario();
      }
    });
  }
  async ngOnInit() {
    const uid = await this.FirebaseauthService.getUid();
    console.log(uid);
    
  }

  initUsuario() {
    this.uid = '';
    this.usuario = {
      uid: '',
      nombre: '',
      email: '',
      contra: '',
    };
    console.log(this.usuario);
  }
  async Registrarse() {
    const credenciales = {
      email: this.usuario.email,
      password: this.usuario.contra,
    };
    const res = await this.FirebaseauthService.registrar(credenciales.email, credenciales.password).catch(err => {
      console.log('error ->', err);
      this.presentToast('Error al registrarse', 2000);
    });
    const uid = await this.FirebaseauthService.getUid();
    this.usuario.uid = uid;
    this.guardarUser();
    console.log(uid);
  }

  async guardarUser() {

    const path = 'Usuarios';
    const name = this.usuario.nombre;
    if (this.newFile !== undefined) {
      const res = await this.FirestorageService.uploadImage(this.newFile, path, name);
    }

    this.database.crearDocument(this.usuario, path, this.usuario.uid).then(res => {
      console.log('!Guardado con Exito!');
      this.presentToast('Registrad@ Correctamente', 2000);
    
    }).catch(error => {
      this.presentToast('Error al Registrarse', 2000);
    });

  }
  async Salir() {
    this.FirebaseauthService.logout();
    this.suscriberUserInfo.unsubscribe();
  }
  getUserInfo(uid: string) {
    console.log('getUserInfo');
    const path = 'Usuarios';
    this.suscriberUserInfo = this.database.getDoc<Usuario>(path, uid).subscribe(res => {
      this.usuario = res;
    });
  }
  ingresar() {

    const credenciales = {
      email: this.usuario.email,
      password: this.usuario.contra,
    };

    this.FirebaseauthService.login(credenciales.email, credenciales.password).then(res => {
      this.router.navigate(['/Home']);
      this.presentToast('Ingreso Con Exito', 2000);

    }).catch(error => {
      this.presentToast('Credenciales Incorrectas', 2000);
    });


  }
  async presentToast(mensaje: string, tiempo: number) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: tiempo
    })
    toast.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Registrando :)',

    });
    await this.loading.present();
    //await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }



}
