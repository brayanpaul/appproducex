import { Categoria } from './../../models/interfaces';
import { BasedatosService } from './../../servicios/basedaros.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/interfaces';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseauthService } from '../../servicios/firebaseauth.service';
import { Subscription } from 'rxjs';
import { MenuController, AlertController, ToastController, LoadingController } from '@ionic/angular';
import { FirestorageService } from '../../servicios/firestorage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  CategoriaSuscribe: Subscription;
  LacteosSuscribe: Subscription;
  SnacksSuscribe: Subscription;
  BebidasSuscribe: Subscription;
  OtrosSuscribe: Subscription;
  Categorias: Producto[] = [];
  items: Producto[] = [];
  itemsC: Categoria[] = [];
  buscar: any;
  loading: any;
  newFile = '';
  newImage = '';
  caducado = true;
  private path = 'Productos/';
  admin = false;

  enablenewEdit = false;
  enableG = true;
  enableP = true;
  newProducto: Producto = {
    id: this.database.creatId(),
    nombre: '',
    descripcion: '',
    fechaEx: '',
    fechaEL: '',
    stock: null,
    precio: null,
    categoria: 'Lacteos',
    foto: '',
    estado: ''


  };
  constructor(public MenuController: MenuController, public BasedatosService: BasedatosService, public Firestore: AngularFirestore,
    public FirebaseauthService: FirebaseauthService, public database: BasedatosService, public loadingController: LoadingController,
    public toastController: ToastController, public alertController: AlertController, public firestorageService: FirestorageService
  ) {

  }
  //funcion que se ejecuta cuando se destruye el componente
  ngOndestroy() {
    if (this.CategoriaSuscribe) {
      this.CategoriaSuscribe.unsubscribe();
    }
    if (this.LacteosSuscribe) {
      this.LacteosSuscribe.unsubscribe();
    }
    if (this.SnacksSuscribe) {
      this.SnacksSuscribe.unsubscribe();
    }

    if (this.BebidasSuscribe) {
      this.BebidasSuscribe.unsubscribe();
    }
    if (this.OtrosSuscribe) {
      this.OtrosSuscribe.unsubscribe();
    }

  }

  ChangeSegment(ev: any) {
    console.log('ChangeSegment()', ev.detail.value);
    const opc = ev.detail.value;
    if (opc == 'Enlatados') {
      this.getEnlatados();
    }
    if (opc == 'Lacteos') {
      this.getLacteos();
    }
    if (opc == 'Snacks') {
      this.getSnacks();
    }
    if (opc == 'Bebidas') {
      this.getBebidas();
    }
    if (opc == 'Otros') {
      this.getOtros();
    }



  }
  async getEnlatados() {
    console.log('getEnlatados()');
    const uid = await this.FirebaseauthService.getUid();
    const path = 'Productos';
    this.LacteosSuscribe = this.BasedatosService.getCollectionQuery<Producto>(path, 'categoria', '==', 'Enlatados ').subscribe(res => {
      if (res.length) {
        res.forEach(producto => {
          producto.fechaEx = new Date(producto.fechaEx.seconds * 1000);
          producto.fechaEL = new Date(producto.fechaEL.seconds * 1000);

          var fecha = new Date();
          console.log('Fecha Actual', fecha);
          console.log('Fecha Expiraciom', producto.fechaEx);

          if (producto.fechaEx < fecha) {
            console.log('Producto caducado');
            this.caducado = true;
            producto.estado = ' Expirado';
            console.log(producto.nombre, "esta expirad@");
          } else {

            console.log('Producto no caducado');
            this.caducado = false;
            producto.estado = '';
          }
        });

        this.Categorias = res;

      }
    });
  }



  async getLacteos() {
    console.log('getLacteos()');
    const uid = await this.FirebaseauthService.getUid();
    const path = 'Productos';
    this.CategoriaSuscribe = this.BasedatosService.getCollectionQuery<Producto>(path, 'categoria', '==', 'Lacteos ').subscribe(res => {
      if (res.length) {
        res.forEach(producto => {
          producto.fechaEx = new Date(producto.fechaEx.seconds * 1000);
          producto.fechaEL = new Date(producto.fechaEL.seconds * 1000);

          var fecha = new Date();
          console.log('Fecha Actual', fecha);
          console.log('Fecha Expiraciom', producto.fechaEx);

          if (producto.fechaEx < fecha) {
            console.log('Producto caducado');
            this.caducado = true;
            producto.estado = ' Expirado';
            console.log(producto.nombre, "esta expirad@");
          } else {

            console.log('Producto no caducado');
            this.caducado = false;
            producto.estado = '';
          }
        });


        this.Categorias = res;

      }
    });
  }

  async getSnacks() {
    console.log('getSnacks()');
    const uid = await this.FirebaseauthService.getUid();
    const path = 'Productos';
    this.SnacksSuscribe = this.BasedatosService.getCollectionQuery<Producto>(path, 'categoria', '==', 'Snacks ').subscribe(res => {
      if (res.length) {
        res.forEach(producto => {
          producto.fechaEx = new Date(producto.fechaEx.seconds * 1000);
          producto.fechaEL = new Date(producto.fechaEL.seconds * 1000);

          var fecha = new Date();
          console.log('Fecha Actual', fecha);
          console.log('Fecha Expiraciom', producto.fechaEx);

          if (producto.fechaEx < fecha) {
            console.log('Producto caducado');
            this.caducado = true;
            producto.estado = ' Expirado';
            console.log(producto.nombre, "esta expirad@");
          } else {

            console.log('Producto no caducado');
            this.caducado = false;
            producto.estado = '';
          }
        });


        this.Categorias = res;

      }
    });
  }
  async getBebidas() {
    console.log('getBebidas()');
    const uid = await this.FirebaseauthService.getUid();
    const path = 'Productos';

    this.BebidasSuscribe = this.BasedatosService.getCollectionQuery<Producto>(path, 'categoria', '==', 'Bebidas ').subscribe(res => {
      if (res.length) {
        res.forEach(producto => {
          producto.fechaEx = new Date(producto.fechaEx.seconds * 1000);
          producto.fechaEL = new Date(producto.fechaEL.seconds * 1000);

          var fecha = new Date();
          console.log('Fecha Actual', fecha);
          console.log('Fecha Expiraciom', producto.fechaEx);

          if (producto.fechaEx < fecha) {
            console.log('Producto caducado');
            this.caducado = true;
            producto.estado = ' Expirado';
            console.log(producto.nombre, "esta expirad@");
          } else {

            console.log('Producto no caducado');
            this.caducado = false;
            producto.estado = '';
          }
        });

        this.Categorias = res;

      }

    });
  }
  async getOtros() {
    console.log('getOtros()');
    const uid = await this.FirebaseauthService.getUid();
    const path = 'Productos';
    this.OtrosSuscribe = this.BasedatosService.getCollectionQuery<Producto>(path, 'categoria', '==', 'Otros ').subscribe(res => {
      if (res.length) {
        res.forEach(producto => {
          producto.fechaEx = new Date(producto.fechaEx.seconds * 1000);
          producto.fechaEL = new Date(producto.fechaEL.seconds * 1000);

          var fecha = new Date();
          console.log('Fecha Actual', fecha);
          console.log('Fecha Expiraciom', producto.fechaEx);

          if (producto.fechaEx < fecha) {
            console.log('Producto caducado');
            this.caducado = true;
            producto.estado = ' Expirado';
            console.log(producto.nombre, "esta expirad@");
          } else {

            console.log('Producto no caducado');
            this.caducado = false;
            producto.estado = '';
          }
        });

        this.Categorias = res;

      }
    });
  }


  async ngOnInit() {
    //this.getItems();
    this.getLacteos();
    this.getItemsC();
    this.getUid();


  }

  getItems() {
    const enlace = 'Productos';
    this.BasedatosService.getCollectionChanges<Producto>(enlace).subscribe(res => {
      this.items = res;

    });
  }


  async addProducto() {
    this.presentLoading();
    const path = 'Productos';
    const name = this.newProducto.nombre;
    const res = await this.firestorageService.uploadImage(this.newFile, path, name);
    this.newProducto.foto = res;
    this.newProducto.fechaEx = new Date(this.newProducto.fechaEx);
    this.newProducto.fechaEL = new Date(this.newProducto.fechaEL);
    console.log(this.newProducto);
    this.database.crearDocument(this.newProducto, this.path, this.newProducto.id).then(res => {
      this.newProducto = {
        id: this.database.creatId(),
        nombre: '',
        descripcion: '',
        fechaEx: '',
        fechaEL: '',
        stock: null,
        precio: null,
        categoria: 'Lacteos',
        foto: '',
        estado: ''
      };
      this.loading.dismiss();
      this.newImage = '';
    }).catch(error => { });
    this.presentToast('Guardado Correctamente', 2000);
  }

  //Carga imagen
  async newImagenUpload(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.newFile = event.target.files[0];
      const reader = new FileReader();
      reader.onload = ((image) => {
        this.newImage = image.target.result as string;
      });
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  async eliminarProducto(producto: Producto) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar',
      message: 'Seguro que desea<strong>...ELIMINAR</strong> Esto',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.BasedatosService.delateDocument(this.path, producto.id);
          }
        }
      ]
    });

    await alert.present();
  }
  getItemsC() {
    const enlace = 'Categoria';
    this.BasedatosService.getCollectionChanges<Categoria>(enlace).subscribe(res => {
      this.itemsC = res;
    });
  }

  async presentToast(mensaje: string, tiempo: number) {
    const toast = await this.toastController.create({
      message: mensaje,
      duration: tiempo
    })
    toast.present();
  }
  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Gargando Producto :)',

    });
    await this.loading.present();
    //await loading.onDidDismiss();
    // console.log('Loading dismissed!');
  }

  getUid() {
    this.FirebaseauthService.stateAuth().subscribe(res => {
      if (res !== null) {
        if (res.uid === 'IKbEOrdloiQXS9V4InK75YquUGC2') {
          this.admin = true;
        } else {
          this.admin = false;
        }
      }
    })

  }

}


