import { ProductosComponent } from './../productos/productos.component';
import { BasedatosService } from './../../servicios/basedaros.service';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { Component, OnInit, Input } from '@angular/core';
import { Producto, Categoria } from '../../models/interfaces';

@Component({
  selector: 'app-lista-p',
  templateUrl: './lista-p.component.html',
  styleUrls: ['./lista-p.component.scss'],
})
export class ListaPComponent implements OnInit {

  @Input() enablenewEdit=false;

  items: Producto[]=[];
  newImage= '';
  newFile='';
//
// Categoria //
categoria: Categoria[] = [];
itemsC: Categoria[] = [];



  loading: any;
  private path='Productos/';
  
  @Input() newProducto: Producto={
    id:this.database.creatId(),
    nombre: '',
    descripcion: '',
    fechaEx: '',
    fechaEL:'' ,
    stock:null,
    precio:null,
    categoria: 'Lacteos',
    foto: '',
    estado:''
  
   };


  constructor(public database: BasedatosService,  public loadingController: LoadingController, 
    public toastController: ToastController,public alertController: AlertController, public BasedatosService: BasedatosService) { }

  ngOnInit() {
    this.getItems();
    // this.getItemsT();
    this.getItemsC();
  }


  //Muestra datos "Categoria"
  // getItemsC() {
  //   const enlace = 'Categoria';
  // }

  // getItemsT(){
  //   const enlace = 'Tienda';
  // }


  getItems(){
    const enlace = 'Productos';
    this.BasedatosService.getCollectionChanges<Producto>(enlace).subscribe(res =>{
    this.items=res;
});
  }

  async eliminarProducto(producto: Producto){
     const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Peligro',
        message: 'Seguro que desea<strong>ELIMINAR</strong> Esto',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Si',
            handler: () => {
              this.BasedatosService.delateDocument(this.path,producto.id);
            }
          }
        ]
      });
  
      await alert.present();
    }


    addProducto(){

      this.presentLoading('Guardando');
      this.database.crearDocument(this.newProducto, this.path,this.newProducto.id).then(res => {
        this.loading.dismiss();
        this.newImage='';
        this.newProducto={
          id: this.database.creatId(),
          nombre: '',
          descripcion: '',
          fechaEx: '',
          fechaEL:'' ,
          stock:null,
          precio:null, 
          categoria: 'Lacteos',
           foto: '',
           estado:''         };
      }).catch(error =>{});
      this.presentToast('Editado Correctamente', 2000 );
    }
    
    async presentToast(mensaje:string, tiempo :number){
      const toast= await this.toastController.create({
        message: mensaje,
        duration:tiempo
      })
      toast.present();
    }

    async presentLoading(message: string) {
      this.loading = await this.loadingController.create({
        cssClass:'my-custom-class',
        message: message,
    
      });
      await this.loading.present();
      //await loading.onDidDismiss();
      // console.log('Loading dismissed!');
    }

    //Carga imagen
async newImagenUpload( event: any){
if(event.target.files && event.target.files[0]){
this.newFile= event.target.files[0];
  const reader= new FileReader();
  reader.onload=((image)=>{
    this.newImage=image.target.result as string;
  });
  reader.readAsDataURL(event.target.files[0]);
}
}
getItemsC() {
  const enlace = 'Categoria';
 this.BasedatosService.getCollectionChanges<Categoria>(enlace).subscribe(res => {
   this.itemsC = res;
  });
}


}
