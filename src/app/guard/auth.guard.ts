import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable} from 'rxjs';
import {map} from "rxjs/operators";
import { AngularFireAuth} from "@angular/fire/auth";
import { isNullOrUndefined } from 'util';
import { Router } from "@angular/router";
@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(private Afauth: AngularFireAuth, private router: Router){}
canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.Afauth.authState.pipe(map(auth =>{
            if(isNullOrUndefined(auth)){
                this.router.navigate(['/Login']);
                return false
            }else{
            return true
            }
        })) 
    }
}
